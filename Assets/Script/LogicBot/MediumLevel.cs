﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumLevel
{
    private List<Vector2> emptyCell = new List<Vector2>();
 
    public void BotMediumLevel(eTarget target , ref CellInBoard[,] _cellInBoard)
    {
        emptyCell.Clear();

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                {
                    emptyCell.Add(new Vector2(i, j));
                }
            }
        }
        if (emptyCell.Count == 0)
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {

            if (AnalizRunPlayerMediumLevel(target , ref _cellInBoard) == true)
            {

            }
            else
            {
                int r = (int)UnityEngine.Random.Range(0, emptyCell.Count - 1);

                if (_cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell == eTarget.empty)
                {
                    _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell = target;

                    _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].OnPointerDown(null);
                }
                /*else
                {
                    switch (_botLevel)
                    {
                        case 1:
                            BotEasyLevel(BotTarget);
                            break;
                        case 2:
                            BotMediumLevel(BotTarget);
                            break;
                        case 3:
                            BotHardLevel(BotTarget);
                            break;
                    }
                }*/
            }

            //After the move bot we need to check the free cells,this may be the last move and maybe is draw. 
            emptyCell.Clear();

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                    {
                        emptyCell.Add(new Vector2(i, j));
                    }
                }
            }
            if (emptyCell.Count == 0)
            {
                ProfilePlayer.EditLastRound(0);

                UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

                UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
            }
        }
    }

    //Field analysis medium level
    private bool AnalizRunPlayerMediumLevel(eTarget target , ref CellInBoard[,] _cellInBoard)
    {
        bool result = false;

        for (int i = 0; i < 3; i++)
        {
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell && _cellInBoard[i, 0].TargetCell != eTarget.empty) //Analiz all vertical line 1 and 2 cells
            {
                if (_cellInBoard[i, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 2].TargetCell = target;

                    _cellInBoard[i, 2].OnPointerDown(null);

                    break;
                }
            }

            if (_cellInBoard[i, 1].TargetCell == _cellInBoard[i, 2].TargetCell && _cellInBoard[i, 2].TargetCell != eTarget.empty) //Analiz all vertical line 2 and 3 cells
            {
                if (_cellInBoard[i, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 0].TargetCell = target;

                    _cellInBoard[i, 0].OnPointerDown(null);
                    break;
                }
            }

            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[0, i].TargetCell != eTarget.empty)//Analiz all horizontal line 1 and 2 cells
            {
                if (_cellInBoard[2, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, i].TargetCell = target;

                    _cellInBoard[2, i].OnPointerDown(null);

                    break;
                }
            }


            if (_cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell && _cellInBoard[1, i].TargetCell != eTarget.empty)//Analiz all horizontal line 2 and 3 cells
            {
                if (_cellInBoard[0, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, i].TargetCell = target;

                    _cellInBoard[0, i].OnPointerDown(null);

                    break;
                }
            }
            //Diagonals
            if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 0].TargetCell != eTarget.empty)//Down from right to left 1 and 2 elements
            {
                if (_cellInBoard[2, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 2].TargetCell = target;

                    _cellInBoard[2, 2].OnPointerDown(null);

                    break;
                }
            }


            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell && _cellInBoard[1, 1].TargetCell != eTarget.empty)//Down from right to left 2 and 3 elements
            {
                if (_cellInBoard[0, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 0].TargetCell = target;

                    _cellInBoard[0, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 2].TargetCell != eTarget.empty)//Up from left to right 1 and 2 elements 
            {
                if (_cellInBoard[2, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 0].TargetCell = target;

                    _cellInBoard[2, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell && _cellInBoard[1, 1].TargetCell != eTarget.empty)//Up from left to right 2 and 3 elements 
            {
                if (_cellInBoard[0, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 2].TargetCell = target;

                    _cellInBoard[0, 2].OnPointerDown(null);

                    break;
                }
            }
        }
        return result;
    }

}
