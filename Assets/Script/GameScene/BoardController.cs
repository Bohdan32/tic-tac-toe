﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class BoardController : MonoBehaviour 
{
    public static BoardController Instance;

    private CellInBoard [,] _cellInBoard = new CellInBoard[3,3];

    [SerializeField]
    private CellInBoard _cellBase;

    private EasyLevel _easyLevel = new EasyLevel();

    private MediumLevel _mediumLevel = new MediumLevel();

    private HardLevel _hardLevel = new HardLevel();

    private float WidthCell;

    private float PaddindCell;

    private GameSession p;

    private eTarget BotTarget;

    private int _botLevel;  

    public int GetLevel
    {
        get
        { 
            return _botLevel;
        }
        set
        { 
            _botLevel = value;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        _botLevel = ProfilePlayer.GetLevel; // this value check level bot;

        WidthCell = _cellBase.transform.GetComponent<SpriteRenderer>().bounds.size.x;

        PaddindCell = WidthCell / 4;

        InstantiateCellInBoard(); // Building cells in field 

        CheckWhoMove(ProfilePlayer.GetLastRound);   // Check who must run first(Bot or Player);   
    }
 
    private void InstantiateCellInBoard()
    {  
        for (int i = 0; i < 3; i++)
        {

            for (int k = 0; k < 3; k++)
            {

            GameObject cell = Instantiate(_cellBase.gameObject, _cellBase.gameObject.transform.parent, false) as GameObject;

            cell.transform.parent = gameObject.transform;

            cell.transform.position = _cellBase.transform.position;

                cell.GetComponent<Transform>().position = (new Vector2(i * (WidthCell + PaddindCell) + _cellBase.GetComponent<Transform>().position.x,
                    -k * (WidthCell + PaddindCell) + _cellBase.GetComponent<Transform>().position.y));
            
                cell.name = k + ":" + i;

                CellInBoard viewCellInBoard = cell.GetComponent<CellInBoard>();

                viewCellInBoard.PositionX = k;

                viewCellInBoard.PositionY = i;

                viewCellInBoard.BoardController = this;

                _cellInBoard[i, k] = viewCellInBoard;

                cell.SetActive(true);
            }
        }
    }

    public bool CheckWinwinningCombination()  // Functions check wins. If function returns 2 - bot wins, if 1 - player wins, in 0 - game continue;
    {
     //  eTarget winningCombination = eTarget.empty;

       for(int i = 0; i< 3; i++)
        {
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell  && _cellInBoard[i, 1].TargetCell== _cellInBoard[i, 2].TargetCell
                && _cellInBoard[i, 2].TargetCell!= eTarget.empty)
            {
                //winningCombination = _cellInBoard[i, 1].TargetCell;

                return true; 
            }

            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell &&
                _cellInBoard[0, i].TargetCell!= eTarget.empty)
            {
                // winningCombination = _cellInBoard[0, i].TargetCell;

                return true;
            }
        }

        //по горизонталі
        if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell &&
            _cellInBoard[0, 0].TargetCell!= eTarget.empty)
        {
            // winningCombination = _cellInBoard[0, 0].TargetCell;

            return true;
        }


        if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell &&
            _cellInBoard[2, 0].TargetCell!= eTarget.empty)
        {
            //winningCombination = _cellInBoard[0, 0].TargetCell;

            return true;
        }

        return false;
    }

    public void CheckWhoMove(int a) // Functions check who run first(Bot or Player). If bot - do first move
    {
        switch (a)
        {
            case 1:
                p = new GameSession(new RunPlayer(), new Cross()); 
                BotTarget = eTarget.zerro;
                ProfileBot.EditMoveLastRound(2);
                break;

            case 2:
                p = new GameSession(new RunBot(), new Cross());
                BotTarget = eTarget.cross;
                ProfileBot.EditMoveLastRound(1);
                switch (_botLevel)
                {
                    case 1:
                        _easyLevel.BotEasyLevel(BotTarget , ref _cellInBoard);
                        break;
                    case 2:
                        _mediumLevel.BotMediumLevel(BotTarget, ref _cellInBoard);
                        break;
                    case 3:
                        _hardLevel.BotHardLevel(BotTarget, ref _cellInBoard);
                        break;
                }
                break;

            case 0:
                
                if (ProfileBot.GetMoveLastRound == 2)
                {
                    p = new GameSession(new RunBot(), new Cross());
                    BotTarget = eTarget.cross;
                    ProfileBot.EditMoveLastRound(1);
                    switch (_botLevel)
                    {
                        case 1:
                            _easyLevel.BotEasyLevel(BotTarget, ref _cellInBoard);
                            break;
                        case 2:
                            _mediumLevel.BotMediumLevel(BotTarget, ref _cellInBoard);
                            break;
                        case 3:
                            _hardLevel.BotHardLevel(BotTarget, ref _cellInBoard);
                            break;
                    }
                }
                else
                {
                    if (ProfileBot.GetMoveLastRound ==1)
                    {
                        p = new GameSession(new RunPlayer(), new Cross());
                        ProfileBot.EditMoveLastRound(2);
                        BotTarget = eTarget.zerro;
                    }   
                }
                break;
        }  
    }

    public Enum GoRun(int pos_x , int pos_y)// Function moves player and bot
    {
        IGameSession _playerNow = p.StateSession;  

        eTarget target = (eTarget)p.Run();

        switch (target)
        {
            case eTarget.cross:

                _cellInBoard[pos_y, pos_x].TargetCell = eTarget.cross;// write result moves 

                break;

            case eTarget.zerro:

                _cellInBoard[pos_y, pos_x].TargetCell = eTarget.zerro;

                break;
        }

        if (CheckWinwinningCombination() && _playerNow.TargetSesion == eTargetSession.player)// Wins Player
        {
            ProfilePlayer.EditLastRound(1);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(1);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

            return target;
        }

        if (CheckWinwinningCombination() && _playerNow.TargetSesion == eTargetSession.bot)// Wins Bot
        {
            ProfilePlayer.EditLastRound(2);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(2);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

            return target;
        }

        if(p.StateSession is RunBot)// if this move no bot we need start move bot
        {
            switch (_botLevel)
            {
                case 1:
                    _easyLevel.BotEasyLevel(BotTarget, ref _cellInBoard);
                    break;
                case 2:
                    _mediumLevel.BotMediumLevel(BotTarget, ref _cellInBoard);
                    break;
                case 3:
                    _hardLevel.BotHardLevel(BotTarget, ref _cellInBoard);
                    break;
            }
        }
       
        return target;
    }


    //Next 2 state change state player and bot: if one of them move croos, other automatically receives zerro. Also we change of moves. 

    // State - 2 
    interface IRun
    {
        eTarget Run(Run run);
    }

    class Zerro : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Cross();
            return eTarget.zerro;
        }
    }

    class Cross : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Zerro();
            return eTarget.cross;
        }  
    }

    class Run
    {
        public IRun State { get; set; }

        public Run(IRun run)
        {
            State = run;
        }

        public eTarget ValueRun()
        {
          return State.Run(this); 
        }
    }

    // State - 1 
    class GameSession
    {
        public IGameSession StateSession{get;set;}

        public Run run;

        public GameSession(IGameSession stateSession , IRun stateRun)
        {
            StateSession = stateSession;

            run = new Run(stateRun);
        }

        public Enum Run()
        {
            StateSession.Run(this); 

            return run.ValueRun();
        }
    }

    interface IGameSession
    {
        void Run(GameSession player );

        eTargetSession TargetSesion { get;}
    }

    class RunPlayer : IGameSession
    {
        public void Run(GameSession player)
        {
            player.StateSession = new RunBot();
        }

        public eTargetSession TargetSesion
        {
            get{ return eTargetSession.player;}
        }
    }

    class RunBot : IGameSession
    {
        public void Run(GameSession player)
        {
            player.StateSession = new RunPlayer();
        }


        public eTargetSession TargetSesion
        {
            get { return eTargetSession.bot; }
        }
    }
}

public enum eTarget
{
    empty,
    cross,
    zerro,
}

public enum eTargetSession
{
    player,
    bot
}















