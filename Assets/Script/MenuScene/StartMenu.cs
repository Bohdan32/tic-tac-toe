﻿using UnityEngine;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour 
{
    [SerializeField]
    private Button _buttonStartGame;

    [SerializeField]
    private Button _buttonExitGame;

	void Start () 
    {
        _buttonStartGame.onClick.AddListener(OpenSelectLevel);

        _buttonExitGame.onClick.AddListener(Exit);
	}
	
    private void OpenSelectLevel()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.SelectLevel);
    }

    private void Exit()
    {
        Application.Quit();
    }
	
}
